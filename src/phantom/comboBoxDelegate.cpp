// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QAbstractItemView>
#include <QStandardItemModel>
#include <QApplication>
#include <QPainter>
#include "comboBoxDelegate.h"



void ComboBoxDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                            const QModelIndex &index) const {

    QStyleOptionMenuItem opt = getStyleOption(option, index);
    painter->fillRect(option.rect, opt.palette.window());
    mCombo->style()->drawControl(QStyle::CE_MenuItem, &opt, painter, mCombo);
}


QStyleOptionMenuItem 
ComboBoxDelegate::getStyleOption(const QStyleOptionViewItem& option,
                                const QModelIndex &index) const {
    QStyleOptionMenuItem menuOption;
    QPalette resolvedpalette = option.palette.resolve(QApplication::palette("QMenu"));
    QVariant value = index.data(Qt::ForegroundRole);

    if (value.canConvert<QBrush>()) {
        resolvedpalette.setBrush(QPalette::WindowText, qvariant_cast<QBrush>(value));
        resolvedpalette.setBrush(QPalette::ButtonText, qvariant_cast<QBrush>(value));
        resolvedpalette.setBrush(QPalette::Text, qvariant_cast<QBrush>(value));
    }
    menuOption.palette = resolvedpalette;
    menuOption.state = QStyle::State_None;
    if (mCombo->window()->isActiveWindow()) {
        menuOption.state = QStyle::State_Active;
    }
    if ((option.state & QStyle::State_Enabled) && (index.model()->flags(index)
                                                    & Qt::ItemIsEnabled)) {
        menuOption.state |= QStyle::State_Enabled;
    } else {
        menuOption.palette.setCurrentColorGroup(QPalette::Disabled);
    }
    if (option.state & QStyle::State_Selected) {
        menuOption.state |= QStyle::State_Selected;
    }
    menuOption.checkType = QStyleOptionMenuItem::Exclusive;
    // a valid checkstate means that the model has checkable items
    const QVariant checkState = index.data(Qt::CheckStateRole);
    if (!checkState.isValid()) {
        menuOption.checked = mCombo->currentIndex() == index.row();
    } else {
        menuOption.checked = qvariant_cast<int>(checkState) == Qt::Checked;
        menuOption.state |= qvariant_cast<int>(checkState) == Qt::Checked
                            ? QStyle::State_On : QStyle::State_Off;
    }
    if (isSeparator(index)) {
        menuOption.menuItemType = QStyleOptionMenuItem::Separator;
    } else {
        menuOption.menuItemType = QStyleOptionMenuItem::Normal;
    }
    QVariant variant = index.model()->data(index, Qt::DecorationRole);
    switch (variant.userType()) {
        case QMetaType::QIcon:
            menuOption.icon = qvariant_cast<QIcon>(variant);
            break;
        case QMetaType::QColor: {
            static QPixmap pixmap(option.decorationSize);
            pixmap.fill(qvariant_cast<QColor>(variant));
            menuOption.icon = pixmap;
            break;
        } default:
            menuOption.icon = qvariant_cast<QPixmap>(variant);
            break;
    }
    if (index.data(Qt::BackgroundRole).canConvert<QBrush>()) {
        menuOption.palette.setBrush(QPalette::All, QPalette::Window,
                    qvariant_cast<QBrush>(index.data(Qt::BackgroundRole)));
    }
    menuOption.text = index.model()->data(index, Qt::DisplayRole).toString()
                            .replace(QLatin1Char('&'), QLatin1String("&&"));
    menuOption.tabWidth = 0;
    menuOption.maxIconWidth =  option.decorationSize.width() + 4;
    menuOption.menuRect = option.rect;
    menuOption.rect = option.rect;
    // Make sure fonts set on the model or on the combo box, in
    // that order, also override the font for the popup menu.
    QVariant fontRoleData = index.data(Qt::FontRole);
    if (fontRoleData.isValid()) {
        menuOption.font = qvariant_cast<QFont>(fontRoleData);
    } else if (mCombo->testAttribute(Qt::WA_SetFont)
            || mCombo->testAttribute(Qt::WA_MacSmallSize)
            || mCombo->testAttribute(Qt::WA_MacMiniSize)
            || mCombo->font() != QApplication::font("QComboBox")) {
        menuOption.font = mCombo->font();
    } else {
        menuOption.font = QApplication::font("QComboMenuItem");
    }
    menuOption.fontMetrics = QFontMetrics(menuOption.font);
    return menuOption;
}

QSize ComboBoxDelegate::sizeHint(const QStyleOptionViewItem& option,
                                const QModelIndex& index) const {
                
    QStyleOptionMenuItem opt = getStyleOption(option, index);
    return mCombo->style()->sizeFromContents(
        QStyle::CT_MenuItem, &opt, option.rect.size(), mCombo);
}