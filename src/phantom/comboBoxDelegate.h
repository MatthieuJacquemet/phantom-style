// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __COMBOBOXDELEGATE_H__
#define __COMBOBOXDELEGATE_H__

#include <QItemDelegate>
#include <QComboBox>
#include <iostream>

class ComboBoxDelegate : public QItemDelegate {

    Q_OBJECT
public:
    inline ComboBoxDelegate(QObject* parent, QComboBox* cmb):
        QItemDelegate(parent), mCombo(cmb) {}

    inline static bool isSeparator(const QModelIndex &index) {
        return index.data(Qt::AccessibleDescriptionRole).toString()
                == QLatin1String("separator");
    }
protected:
    QStyleOptionMenuItem getStyleOption(const QStyleOptionViewItem& option,
                                        const QModelIndex& index) const;

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const override;

    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index) const override;
private:
    QComboBox* mCombo;
};


#endif // __COMBOBOXDELEGATE_H__